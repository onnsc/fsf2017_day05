//load libraries
var express=require("express");
var path=require("path");

//create an instance of the app
var app = express();

//setting up the routes
app.use(express.static(path.join(__dirname,"public")));
app.use("/libs", express.static(path.join(__dirname,"bower_components")));

//setting up the port
app.set("port",parseInt(process.argv[2]) || 3000);

//start the server
app.listen(app.get("port"),function(){
    console.log("Application loading port %d at %s", app.get("port"), new Date());
})